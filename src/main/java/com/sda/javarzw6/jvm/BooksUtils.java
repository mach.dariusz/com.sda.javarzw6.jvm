package com.sda.javarzw6.jvm;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class BooksUtils {

    public static String readFile(String fileName, int method) throws IOException {

        switch (method) {
            case 0:
                return readFileBeforeJava7(fileName);
            case 1:
                return readFileJava7(fileName);
            case 2:
                return readFileJava7WithCharacterEncoding(fileName);
            case 3:
                return readFileJava8(fileName);

            default:
                return readFileBeforeJava7(fileName);
        }
    }

    /**
     * how to do it using java less than 7
     *
     * @param fileName
     * @return parsed file as String
     * @throws IOException
     */
    private static String readFileBeforeJava7(String fileName) throws IOException {

        InputStream is = new FileInputStream(fileName);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));

        String line = buf.readLine();
        StringBuilder sb = new StringBuilder();
        while (line != null) {
            sb.append(line).append("\n");
            line = buf.readLine();
        }
        String fileAsString = sb.toString();
        System.out.println("Contents (before Java 7) : " + fileAsString);

        return fileAsString;
    }

    /**
     * how to do it using java 7
     *
     * @param fileName
     * @return parsed file as String
     * @throws IOException
     */
    private static String readFileJava7(String fileName) throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get(fileName)));
        System.out.println("Contents (Java 7) : " + contents);

        return contents;
    }

    /**
     * how to do it using java 7 with character encoding
     *
     * @param fileName
     * @return parsed file as String
     * @throws IOException
     */
    private static String readFileJava7WithCharacterEncoding(String fileName) throws IOException {
        String fileString = new String(Files.readAllBytes(Paths.get(fileName)), StandardCharsets.UTF_8);
        System.out.println("Contents (Java 7 with character encoding ) : " + fileString);

        return fileString;
    }

    /**
     * how to do it using java 8
     *
     * @param fileName
     * @return parsed file as String
     * @throws IOException
     */
    private static String readFileJava8(String fileName) throws IOException {
        String content = Files.lines(Paths.get(fileName), StandardCharsets.UTF_8).collect(Collectors.joining("\n"));

        System.out.println("Contents (Java 8) : " + content);

        return content;
    }

}
