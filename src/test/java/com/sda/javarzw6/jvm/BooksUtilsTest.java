package com.sda.javarzw6.jvm;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class BooksUtilsTest {

    @DisplayName("Test should read file and return String - before Java 7")
    @Test
    void test_v1() throws IOException {

        String content = BooksUtils.readFile("PanTadeusz.txt", 0);

        Assertions.assertEquals(
                84316,
                content.split("\\W+").length
        );
    }

    @DisplayName("Test should read file and return String - Java 7")
    @Test
    void test_v2() throws IOException {

        String content = BooksUtils.readFile("PanTadeusz.txt", 1);

        Assertions.assertEquals(
                84316,
                content.split("\\W+").length
        );
    }
    @DisplayName("Test should read file and return String - Java 7 with encoding")
    @Test
    void test_v3() throws IOException {

        String content = BooksUtils.readFile("PanTadeusz.txt", 2);

        Assertions.assertEquals(
                84316,
                content.split("\\W+").length
        );
    }

    @DisplayName("Test should read file and return String - Java 8")
    @Test
    void test_v4() throws IOException {

        String content = BooksUtils.readFile("PanTadeusz.txt", 3);

        Assertions.assertEquals(
                84316,
                content.split("\\W+").length
        );
    }
}