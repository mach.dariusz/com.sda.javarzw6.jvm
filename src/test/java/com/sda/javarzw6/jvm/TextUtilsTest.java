package com.sda.javarzw6.jvm;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class TextUtilsTest {

    @DisplayName("Read text from file and check count of words")
    @Test
    void readTextAndCheckCountOfWords_v1() throws IOException {

        String text = TextUtils.readText("PanTadeusz.txt", 0);

        Assertions.assertEquals(text.split("\\W+").length, 84316);
    }

    @DisplayName("Read text from file and check count of words")
    @Test
    void readTextAndCheckCountOfWords_v2() throws IOException {

        String text = TextUtils.readText("PanTadeusz.txt", 1);

        Assertions.assertEquals(text.split("\\W+").length, 84316);
    }

    @DisplayName("Read text from file and check count of words")
    @Test
    void readTextAndCheckCountOfWords_v3() throws IOException {

        String text = TextUtils.readText("PanTadeusz.txt", 2);

        Assertions.assertEquals(text.split("\\W+").length, 84316);
    }

    @DisplayName("Read text from file and check count of words")
    @Test
    void readTextAndCheckCountOfWords_v4() throws IOException {

        String text = TextUtils.readText("PanTadeusz.txt", 3);

        Assertions.assertEquals(text.split("\\W+").length, 84316);
    }
}